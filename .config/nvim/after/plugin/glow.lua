require('glow').setup({
  border = ""
})

-- Show project readme
vim.keymap.set("n", "<leader>gr", "<cmd>Glow README.md<CR>")

-- Show current file
vim.keymap.set("n", "<leader>gc", "<cmd>Glow %<CR>")
