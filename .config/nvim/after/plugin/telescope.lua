require('telescope').setup {
  extensions = {
    file_browser = {
      hidden = true
    }
  }
}

require("telescope").load_extension("noice")

local builtin = require('telescope.builtin')

vim.keymap.set('n', '<C-p>', builtin.find_files, {})
vim.keymap.set('n', '<leader>hf', function() builtin.find_files({ hidden = true }) end, {})
vim.keymap.set('n', '<leader>sf', function() builtin.find_files({ hidden = true, cwd = vim.fn.expand('%:p:h') }) end, {})
vim.keymap.set('n', '<leader>gf', builtin.git_files, {})
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
-- vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
vim.keymap.set('n', '<leader>td', builtin.diagnostics, {})
