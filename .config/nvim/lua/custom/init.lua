require("custom.opts")
require("custom.remap")

vim.cmd [[ highlight link @keyword.import.rust @keyword ]]
vim.cmd [[ highlight link @module.rust @keyword ]]

-- Temporary workaround, fixed in neovim nightly.
for _, method in ipairs({ 'textDocument/diagnostic', 'workspace/diagnostic' }) do
  local default_diagnostic_handler = vim.lsp.handlers[method]
  vim.lsp.handlers[method] = function(err, result, context, config)
    if err ~= nil and err.code == -32802 then
      return
    end
    return default_diagnostic_handler(err, result, context, config)
  end
end
