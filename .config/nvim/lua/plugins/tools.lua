return {
  { 'lewis6991/gitsigns.nvim' },
  {
    'windwp/nvim-autopairs',
    event = 'InsertEnter',
    config = function()
      local rule = require('nvim-autopairs.rule')
      local npairs = require('nvim-autopairs')

      npairs.setup()
      npairs.add_rule(rule("<", ">"))
    end,
  },
  { 'numToStr/Comment.nvim' },
  { 'christoomey/vim-tmux-navigator' },
  {
    "kylechui/nvim-surround",
    version = "*",
    event = "VeryLazy",
    config = true,
  },
  {
    'ellisonleao/glow.nvim',
    config = true,
    cmd = 'Glow'
  },
  {
    'folke/noice.nvim',
    event = 'VeryLazy',
    dependencies = {
      'MunifTanjim/nui.nvim',
    }
  },
  {
    "folke/snacks.nvim",
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    priority = 1000,
    lazy = false,
    opts = {
      bigfile = { enabled = true },
      notifier = { enabled = true },
      quickfile = { enabled = true },
      statuscolumn = { enabled = true, },
      words = { enabled = true, modes = { "n", "c" }, },
      styles = {
        notification = {
          wo = { wrap = true } -- Wrap notifications
        }
      }
    },
    keys = {
      { '<leader>nh', function() Snacks.notifier.show_history() end,   desc = "Notification History" },
      { "<leader>nd", function() Snacks.notifier.hide() end,           desc = "Dismiss All Notifications" },
      { "<leader>.",  function() Snacks.scratch() end,                 desc = "Toggle Scratch Buffer" },
      { "<leader>S",  function() Snacks.scratch.select() end,          desc = "Select Scratch Buffer" },
      { "]]",         function() Snacks.words.jump(vim.v.count1) end,  desc = "Next Reference",           mode = { "n", "t" } },
      { "[[",         function() Snacks.words.jump(-vim.v.count1) end, desc = "Prev Reference",           mode = { "n", "t" } },

    },
  },
  {
    'folke/todo-comments.nvim',
    dependencies = { 'nvim-lua/plenary.nvim' },
    opts = {},
  },
  { 'junegunn/fzf' },
  { 'junegunn/fzf.vim' },
  { 'Glench/Vim-Jinja2-Syntax' },
  { 'tpope/vim-abolish' },
  {
    'MeanderingProgrammer/render-markdown.nvim',
    dependencies = { 'nvim-treesitter/nvim-treesitter', 'nvim-tree/nvim-web-devicons' },
    ---@module 'render-markdown'
    ---@type render.md.UserConfig
    opts = {},
  }
}
