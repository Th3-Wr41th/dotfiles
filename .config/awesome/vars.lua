return {
  -- Apps
  terminal = os.getenv("TERMINAL") or "alacritty",
  editor = os.getenv("EDITOR") or "nvim",
  browser = os.getenv("BROWSER") or "firefox",
  alt_browser = os.getenv("ALT_BROWSER") or "mullvad-browser",
  file_manager = os.getenv("FILE_MANAGER") or "alacritty -e yazi",
  -- Mod Keys
  modkey = "Mod4",
  altkey = "Mod1"
}
