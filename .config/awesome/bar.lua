-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")

-- Theme handling library
local beautiful = require("beautiful")
--
-- local theme_path = require("theme").theme_path
-- beautiful.init(theme_path)

-- Widget and layout library
local wibox = require("wibox")

local modkey = require("vars").modkey

--- Wibar

local function set_wallpaper(s)
  -- Wallpaper
  if beautiful.wallpaper then
    local wallpaper = beautiful.wallpaper
    -- If wallpaper is a function, call it with the screen
    if type(wallpaper) == "function" then
      wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)
  end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

local taglist_buttons = gears.table.join(
  awful.button({}, 1,
    function(t)
      t:view_only()
    end
  ),
  awful.button({ modkey }, 1,
    function(t)
      if client.focus then
        client.focus:move_to_tag(t)
      end
    end
  ),
  awful.button({}, 3, awful.tag.viewtoggle),
  awful.button({ modkey }, 3,
    function(t)
      if client.focus then
        client.focus:toggle_tag(t)
      end
    end
  ),
  awful.button({}, 4,
    function(t)
      awful.tag.viewnext(t.screen)
    end
  ),
  awful.button({}, 5,
    function(t)
      awful.tag.viewprev(t.screen)
    end
  )
)

-- Create a wibox for each screen and add it
local bar = function(s)
  -- Wallpaper
  set_wallpaper(s)

  -- Each screen has its own tag table.
  awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

  -- Create a promptbox for each screen
  s.mypromptbox = awful.widget.prompt()

  -- Create a taglist widget
  s.mytaglist = awful.widget.taglist {
    screen = s,
    filter = function(t) return t.selected or #t:clients() > 0 end,
    buttons = taglist_buttons
  }

  s.myclock = wibox.widget.textclock("%b %d %H:%M:%S", 1)

  -- Create the wibox
  s.mywibox = awful.wibar({ position = "top", screen = s })
  -- Add widgets to the wibox
  s.mywibox:setup {
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      s.mytaglist,
      s.mypromptbox,
    },
    { -- Middle
      layout = wibox.layout.flex.horizontal,
    },
    { -- Right widgets
      layout = wibox.layout.fixed.horizontal,
      spacing = 5,
      s.myclock,
      wibox.widget.systray()
    },
  }
end

return {
  bar = bar
}
