local themes = {
  "default",
  "onedark"
}

local chosen_theme = themes[2]

return {
  theme_path = string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), chosen_theme)
}
