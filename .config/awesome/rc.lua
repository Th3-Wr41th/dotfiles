-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local awful = require("awful")
require("awful.autofocus")

-- Theme handling library
local beautiful = require("beautiful")

-- Notification library
local naughty = require("naughty")

-- Other Libraries
local menubar = require("menubar")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")
-- Add some visual indicators for common window management operations
require("collision")()

-- My Libraries
local vars = require("vars")
local keybindings = require("keybindings")
local rules = require("rules")
local bar = require("bar").bar

--- Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
  naughty.notify({
    preset = naughty.config.presets.critical,
    title = "Oops, there were errors during startup!",
    text = awesome.startup_errors
  })
end

-- Handle runtime errors after startup
do
  local in_error = false
  awesome.connect_signal("debug::error", function(err)
    -- Make sure we don't go into an endless error loop
    if in_error then return end
    in_error = true

    naughty.notify({
      preset = naughty.config.presets.critical,
      title = "Oops, an error happened!",
      text = tostring(err)
    })
    in_error = false
  end)
end
---

--- Variable definitions
-- Themes define colours, icons, font and wallpapers.
local theme = require("theme")

beautiful.init(theme.theme_path)

-- Layouts
awful.layout.layouts = {
  awful.layout.suit.tile,
}
---

-- Menubar configuration
menubar.utils.terminal = vars.terminal -- Set the terminal for applications that require it
---

awful.screen.connect_for_each_screen(bar)

--- Key bindings
globalkeys = keybindings.globalkeys
clientkeys = keybindings.clientkeys

-- Set keys
root.keys(globalkeys)
---

--- Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = rules.rules
---

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- QOL
beautiful.useless_gap = 3

-- Autostart
awful.spawn.with_shell("compton")
awful.spawn.with_shell("nm-applet")
-- awful.spawn.with_shell("pgrep battery-info > /dev/null || battery-info-daemon &")
awful.spawn.with_shell("pgrep udiskie > /dev/null || udiskie -t &")
