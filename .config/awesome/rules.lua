-- Standard awesome library
local gears = require("gears")
local awful = require("awful")

-- Theme handling library
local beautiful = require("beautiful")

-- Themes define colours, icons, font and wallpapers.
beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")

-- My Libraries
local modkey = require("vars").modkey
local clientkeys = require("keybindings").clientkeys

local clientbuttons = gears.table.join(
  awful.button(
    {},
    1,
    function(c)
      c:emit_signal("request::activate", "mouse_click", { raise = true })
    end
  ),
  awful.button(
    { modkey },
    1,
    function(c)
      c:emit_signal("request::activate", "mouse_click", { raise = true })
      awful.mouse.client.move(c)
    end
  ),
  awful.button(
    { modkey },
    3,
    function(c)
      c:emit_signal("request::activate", "mouse_click", { raise = true })
      awful.mouse.client.resize(c)
    end
  )
)

local rules = {
  -- All clients will match this rule.
  {
    rule = {},
    properties = {
      border_width = beautiful.border_width,
      border_color = beautiful.border_normal,
      focus = awful.client.focus.filter,
      raise = true,
      keys = clientkeys,
      buttons = clientbuttons,
      screen = awful.screen.preferred,
      placement = awful.placement.no_overlap + awful.placement.no_offscreen
    }
  },
  -- Floating clients.
  {
    rule_any = {
      instance = {
        "DTA",   -- Firefox addon DownThemAll.
        "copyq", -- Includes session name in class.
        "pinentry",
      },
      class = {
        "Arandr",
        "Blueman-manager",
        "Gpick",
        "Kruler",
        "MessageWin",  -- kalarm.
        "Sxiv",
        "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
        "Wpa_gui",
        "veromix",
        "xtightvncviewer"
      },
      name = {
        "Event Tester", -- xev.
      },
      role = {
        "AlarmWindow",   -- Thunderbird's calendar.
        "ConfigManager", -- Thunderbird's about:config.
        "pop-up",        -- e.g. Google Chrome's (detached) Developer Tools.
      }
    },
    properties = {
      floating = true
    }
  },
  -- Open Browser on tab 2 of screen 1
  {
    rule_any = {
      class = {
        "Navigator",
      },
      role = {
        "browser",
      }
    },
    except_any = {
      class = {
        "draw.io",
      }
    },
    properties = {
      screen = 1,
      tag = "2",
    }
  },
  -- Force apps to not float
  {
    rule_any = {
      class = {
        "firefox",
        "kicad",
        "vlc",
        "Navigator",
        "brave-browser",
        "Brave-browser",
        "Blender"
      },
    },
    properties = {
      opacity = 1,
      maximized = false,
      floating = false
    }
  },
}

return {
  rules = rules
}
