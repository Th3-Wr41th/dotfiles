# Dotfiles

## Dependencies

```
advcpmv
alacritty
awesomewm
distrobox
fzf
git
neovim
oh-my-posh
rofi
rustup / cargo
tmux
udiskie
zsh
```

```
$ cargo install bat eza fd-find ripgrep tlrc zoxide
```

### Fonts

Default font is the patched nerd font SourceCodePro. Avaiable at
[nerdfonts.com](nerdfonts.com). Any patched nerd font will work but you will
need to update the [fontconfig](.config/fontconfig/fonts.conf) for your chosen
font.

## Install

```
$ git clone --bare https://gitlab.com/Th3-Wr41th/dotfiles.git .dotfiles.git
$ git --git-dir=$HOME/.dotfiles.git --work-tree=$HOME checkout
$ dotfiles lfs fetch
$ dotfiles lfs checkout
$ dotfiles submodule init
$ dotfiles submodule update
$ dotfiles config status.showUntrackedFiles no

$ ./${HOME}/.local/share/tmux/plugins/tpm/scripts/install_plugins.sh
$ nvim --headless "+Lazy! sync" +qa
```
