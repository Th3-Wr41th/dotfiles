#!/usr/bin/env sh

BOLD=$(tput bold)
NORMAL=$(tput sgr0)

cat << EOF
${BOLD}feat${NORMAL} – a new feature is introduced with the changes

${BOLD}fix${NORMAL} – a bug fix has occurred

${BOLD}chore${NORMAL} – changes that do not relate to a fix or feature and don't modify src or test files (for example updating dependencies)

${BOLD}refactor${NORMAL} – refactored code that neither fixes a bug nor adds a feature

${BOLD}docs${NORMAL} – updates to documentation such as a the README or other markdown files

${BOLD}style${NORMAL} – changes that do not affect the meaning of the code, likely related to code formatting such as white-space, missing semi-colons, and so on.

${BOLD}test${NORMAL} – including new or correcting previous tests

${BOLD}perf${NORMAL} – performance improvements

${BOLD}ci${NORMAL} – continuous integration related

${BOLD}build${NORMAL} – changes that affect the build system or external dependencies

${BOLD}revert${NORMAL} – reverts a previous commit
EOF
